let lib_vparser_fb2 = require('viva-parser-fb2')
let lib_fs = require('fs')
const lib_os = require('os')

let parser = new lib_vparser_fb2()
parser.parse(lib_fs.readFileSync('... path for fb2 file ...','utf8'))

let a_1 = parser.get_formatted_annotation({format: 'plain'})
let t_1 = parser.get_formatted_text({format: 'plain'})
lib_fs.writeFileSync('fb2.txt',''.concat(a_1, lib_os.EOL, t_1))

let a_2 = parser.get_formatted_annotation({format: 'markdown'})
let t_2 = parser.get_formatted_text({format: 'markdown'})
lib_fs.writeFileSync('fb2.md',''.concat(a_2, lib_os.EOL, t_2))