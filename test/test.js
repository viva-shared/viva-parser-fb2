// @ts-check

const assert = require('assert');
const testing_lib = require('./../index.js')
const lib_fs = require('fs')
const lib_path = require('path')
const lib_os = require('os')

describe("fb2", function() {
    it('good fb2', function() {
        let lib = new testing_lib()
        let plain = lib_fs.readFileSync(lib_path.join(__dirname, 'test0_plain.txt'),'utf8')
        assert.equal(lib.parse(lib_fs.readFileSync(lib_path.join(__dirname, 'test0.fb2'),'utf8')), true)
        assert.equal(''.concat(
            lib.get_formatted_annotation({format: 'plain', eol: false, indent: '  '}),
            lib_os.EOL,
            lib.get_formatted_text({format: 'plain', eol: false, indent: '  '})
        ), plain)
    })

    it('bad fb2 - image', function() {
        let lib = new testing_lib()
        assert.equal(lib.parse(lib_fs.readFileSync(lib_path.join(__dirname, 'test1.fb2'),'utf8')), false)
        assert.equal(lib.get_formatted_annotation({format: 'plain', eol: false, indent: '  '}), '')
        assert.equal(lib.get_formatted_text({format: 'plain', eol: false, indent: '  '}), '')
    })

    it('bad fb2 - empty txt file', function() {
        let lib = new testing_lib()
        assert.equal(lib.parse(lib_fs.readFileSync(lib_path.join(__dirname, 'test2.fb2'),'utf8')), false)
        assert.equal(lib.get_formatted_annotation({format: 'plain', eol: false, indent: '  '}), '')
        assert.equal(lib.get_formatted_text({format: 'plain', eol: false, indent: '  '}), '')
    })
})