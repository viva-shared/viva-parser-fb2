// @ts-check
/**
 * @license MIT
 * @author Vitalii vasilev
*/

/** @private */
const lib_vconv = require('viva-convert')
/** @private */
const lib_sax = require('sax')
/** @private */
const lib_os = require('os')

/**
 * @typedef type_author
 * @property {string} last_name
 * @property {string} middle_name
 * @property {string} first_name
 */
/**
 * @typedef type_sequence
 * @property {string} title series name
 * @property {number} position position in series
 */
/**
 * @typedef type_text
 * @property {string} path xml path
 * @property {Object[]} style_list styles
 * @property {string} data piece of text
 * @property {Object} link link to note
 * @property {boolean} new_line_before add or not new line before this piece of text
 */
/**
 * @typedef type_link
 * @property {string} id id link
 * @property {string} path xml path
 * @property {Object[]} style_list styles
 * @property {string} data piece of link
 * @property {boolean} new_line_before add or not new line before this piece of link
 */
/**
 * @typedef type_binary
 * @property {string} id id binary resource
 * @property {string} content_type content type
 * @property {string} data binary value
 */
/**
 * @typedef type_book
 * @property {string} title
 * @property {string} subtitle
 * @property {string} note
 * @property {type_author} origin_author
 * @property {string} origin_language
 * @property {type_author} translator_author
 * @property {string} translator_language
 * @property {type_sequence[]} sequence_list
 * @property {string[]} genre_list
 * @property {string[]} keyword_list
 * @property {string} isbn
 * @property {string} cover_binary_id
 * @property {type_text[]} annotation
 * @property {type_text[]} text
 * @property {type_link[]} link
 * @property {type_binary[]} binary
 */
/**
 * @typedef type_format format export text
 * @property {string} [format] 'plain' or 'markdown', default 'plain'
 * @property {string} [indent] indent for new paragraph, default '  ' (2 spaces)
 * @property {boolean} [eol] allow end-of-line inside paragraph, default false
 */

module.exports = Parser_fb2

/**
* @class  (license MIT) parse text from fb2 format, example - see example.js
*/
function Parser_fb2() {
    if (!(this instanceof Parser_fb2)) return new Parser_fb2()
}

/**
 * result parse fb2-formatted text
 * @type {type_book}
 */
Parser_fb2.prototype.book = undefined
/**
 * rlist of parsing error
 * @type {Object[]}
 */
Parser_fb2.prototype.parse_error_list = []

/**
 * parse fb2-formatted text
 * @param {string} text fb2-formatted text
 * @returns {boolean} fb2 or not fb2
 */
Parser_fb2.prototype.parse = function (text) {
    this.parse_error_list = []
    let find_fb2 = false
    let self = this
    self.book = {
        title: undefined,
        subtitle: undefined,
        note: undefined,
        origin_author: {
            first_name: undefined,
            middle_name: undefined,
            last_name: undefined
        },
        origin_language: undefined,
        translator_author: {
            first_name: undefined,
            middle_name: undefined,
            last_name: undefined
        },
        translator_language: undefined,
        sequence_list: [],
        genre_list: [],
        keyword_list: [],
        isbn: undefined,
        cover_binary_id: undefined,
        annotation: [],
        text: [],
        link: [],
        binary: []
    }
    try {
        if (!text.substring(0, 500).toLowerCase().includes('fictionbook')) {
            return find_fb2
        }

        let parser = lib_sax.parser(false, undefined)

        /** @private @type {string} */
        let state = undefined
        /** @private @type {string} */
        let body_name = undefined
        /** @private @type {string} */
        let body_note_id = undefined
        /** @private @type {string} */
        let binary_id = undefined
        /** @private @type {string} */
        let binary_content_type = undefined
        /** @private @type {Object[]} */
        let styles = []
        /** @private @type {Object} */
        let link = undefined
        /** @private @type {Object} */
        let cover = undefined
        /** @private @type {boolean} */
        let new_line_before = undefined

        parser.onerror = function (e) {
            self.parse_error_list.push(e)
        }

        parser.ontext = function (t) {
            if (lib_vconv.isAbsent(state) || lib_vconv.isEmpty(t)) return
            let find_fb2_chunk = true
            if (state === '.fictionbook.description.title-info.genre') {
                t = t.trim()
                if (!self.book.genre_list.some(f => f === t)) self.book.genre_list.push(t)
            } else if (state === '.fictionbook.description.title-info.author.first-name') {
                self.book.origin_author.first_name = t.trim()
            } else if (state === '.fictionbook.description.title-info.author.middle-name') {
                self.book.origin_author.middle_name = t.trim()
            } else if (state === '.fictionbook.description.title-info.author.last-name') {
                self.book.origin_author.last_name = t.trim()
            } else if (state === '.fictionbook.description.title-info.book-title') {
                self.book.title = t.trim()
            } else if (state === '.fictionbook.description.title-info.lang') {
                self.book.origin_language = t.trim()
            } else if (state === '.fictionbook.description.title-info.translator.first-name') {
                self.book.translator_author.first_name = t.trim()
            } else if (state === '.fictionbook.description.title-info.translator.middle-name') {
                self.book.translator_author.middle_name = t.trim()
            } else if (state === '.fictionbook.description.title-info.translator.last-name') {
                self.book.translator_author.last_name = t.trim()
            } else if (state === '.fictionbook.description.title-info.src-lang') {
                self.book.translator_language = t.trim()
            } else if (state === '.fictionbook.description.publish-info.isbn') {
                self.book.isbn = t.trim()
            } else if (state === '.fictionbook.description.title-info.keywords') {
                self.book.keyword_list = Array.from(new Set(
                    t
                    .split(',')
                    .filter(f => !lib_vconv.isEmpty(f))
                    .map(f => { return lib_vconv.toString(f.trim())})
                ))
            } else if (state === '.fictionbook.description.custom-info') {
                self.book.note = t.trimRight()
            } else if (state.substring(0, 17) === '.fictionbook.body') {
                let data = lib_vconv.toString(t,'').trimRight()
                let small_state = state.substring(17, state.length)
                let style_list = styles.map(m => {return m})

                if (body_name === 'notes') {

                    if ((style_list.length > 0 || !lib_vconv.isAbsent(link)) && !check_delimiter_char(data.substring(0,1)) && self.book.link.length > 0 && !new_line_before) {
                        let last = self.book.link[self.book.link.length - 1]
                        if (!check_delimiter_char(last.data.substring(last.data.length - 1, last.data.length))) {
                            data = ' '.concat(data)
                        }
                    }

                    self.book.link.push({
                        data: data,
                        path: small_state,
                        new_line_before: new_line_before,
                        style_list: (style_list.length > 0 ? style_list : undefined),
                        id: body_note_id
                    })

                } else {

                    if ((style_list.length > 0 || !lib_vconv.isAbsent(link)) && !check_delimiter_char(data.substring(0,1)) && self.book.text.length > 0 && !new_line_before) {
                        let last = self.book.text[self.book.text.length - 1]
                        if (!check_delimiter_char(last.data.substring(last.data.length - 1, last.data.length))) {
                            data = ' '.concat(data)
                        }
                    }

                    self.book.text.push({
                        data: data,
                        path: small_state,
                        new_line_before: new_line_before,
                        link: link,
                        style_list: (style_list.length > 0 ? style_list : undefined),
                    })
                }
            } else if (state.substring(0, 46) === '.fictionbook.description.title-info.annotation') {
                let data = lib_vconv.toString(t,'').trimRight()
                let small_state = state.substring(46, state.length)
                let style_list = styles.map(m => {return m})

                if ((style_list.length > 0 || !lib_vconv.isAbsent(link)) && !check_delimiter_char(data.substring(0,1)) && self.book.annotation.length > 0 && !new_line_before) {
                    let last = self.book.annotation[self.book.annotation.length - 1]
                    if (!check_delimiter_char(last.data.substring(last.data.length - 1, last.data.length))) {
                        data = ' '.concat(data)
                    }
                }

                self.book.annotation.push({
                    data: data,
                    path: small_state,
                    new_line_before: new_line_before,
                    link: link,
                    style_list: (style_list.length > 0 ? style_list : undefined)
                })
            } else if (state === '.fictionbook.binary') {
                self.book.binary.push({
                    id: binary_id,
                    content_type: binary_content_type,
                    data: t
                })
            } else {
                find_fb2_chunk = false
                // NOT USED TAGS
                // console.log('state:     ',state)
                // console.log('text:      ',t)
            }
            if (find_fb2_chunk === true) {
                find_fb2 = true
            }
        }

        parser.onopentag = function (node) {
            if (lib_vconv.isAbsent(state)  && node.name.toLowerCase() === 'fictionbook') {
                state = ''
            }
            if (lib_vconv.isAbsent(state) || lib_vconv.isEmpty(node.name)) return

            new_line_before = false
            if (node.name.toLowerCase() === 'style') {
                let style = {}
                if (!lib_vconv.isAbsent(node.attributes)) {
                    for (let key in node.attributes) {
                        style[key.toLowerCase()] = node.attributes[key]
                    }
                }
                styles.push(style)
            } else if (node.name.toLowerCase() === 'a') {
                if (!lib_vconv.isAbsent(node.attributes)) {
                    link = {}
                    for (let key in node.attributes) {
                        link[key.toLowerCase()] = node.attributes[key]
                    }
                }
            } else {
                state = state.concat('.',node.name.trim().toLowerCase())
                new_line_before = true
            }

            if (body_name === 'notes') {
                let id = lib_vconv.toString(lib_vconv.findPropertyValueInObject(node.attributes,'id'))
                if (!lib_vconv.isAbsent(id)) {
                    body_note_id = id
                }
            }

            if (state === '.fictionbook.body' && !lib_vconv.isAbsent(node.attributes)) {
                body_name = lib_vconv.toString(lib_vconv.findPropertyValueInObject(node.attributes, 'name'))
                if (!lib_vconv.isEmpty(body_name)) {
                    body_name = body_name.trim().toLowerCase()
                }
            } else if (state === '.fictionbook.binary' && !lib_vconv.isAbsent(node.attributes)) {
                binary_id = lib_vconv.toString(lib_vconv.findPropertyValueInObject(node.attributes, 'id'))
                binary_content_type = lib_vconv.toString(lib_vconv.findPropertyValueInObject(node.attributes, 'content-type'))
            } else if (state === '.fictionbook.description.title-info.sequence' && !lib_vconv.isAbsent(node.attributes)) {
                let title = lib_vconv.toString(lib_vconv.findPropertyValueInObject(node.attributes,'name'))
                if (!lib_vconv.isEmpty(title) && !self.book.sequence_list.some(f => f.title.toLowerCase() === title.trim().toLowerCase())) {
                    self.book.sequence_list.push({
                        title: title.trim(),
                        position: lib_vconv.toInt(lib_vconv.findPropertyValueInObject(node.attributes,'number'))
                    })
                }
            } else if (state === '.fictionbook.description.title-info.coverpage.image' && !lib_vconv.isAbsent(node.attributes)) {
                cover = {}
                for (let key in node.attributes) {
                    cover[key.toLowerCase()] = node.attributes[key]
                }
            }
        }

        parser.onclosetag = function (node) {
            if (lib_vconv.isAbsent(state) || lib_vconv.isEmpty(node)) return
            if (node.toLowerCase() === 'style') {
                styles.pop()
            } else if (node.toLowerCase() === 'a') {
                link = undefined
            } else {
                let i = state.lastIndexOf('.')
                if (i >= 0) {
                    state = state.substring(0, i)
                }
            }
        }

        parser.onend = function () {
            if (!lib_vconv.isAbsent(cover) && self.book.binary.length > 0) {
                let find_cover_link = lib_vconv.toString(lib_vconv.findPropertyValueInObject(cover,'xlink:href'))
                if (lib_vconv.isAbsent(find_cover_link)) {
                    find_cover_link = lib_vconv.toString(lib_vconv.findPropertyValueInObject(cover,'l:href'))
                }
                if (!lib_vconv.isEmpty(find_cover_link)) {
                    find_cover_link = lib_vconv.border_del(find_cover_link, '#', undefined)
                    let find_cover = self.book.binary.find(f => !lib_vconv.isEmpty(f.id) && f.id.trim().toLowerCase() === find_cover_link.trim().toLowerCase())
                    if (!lib_vconv.isAbsent(find_cover)) {
                        self.book.cover_binary_id = find_cover.id
                    }
                }
            }
            if (self.book.annotation.length > 0) {
                self.book.annotation[0].new_line_before = false
            }
            if (self.book.text.length > 0) {
                self.book.text[0].new_line_before = false
            }
            if (self.book.link.length > 0) {
                self.book.link[0].new_line_before = false
            }
        }

        parser.write(text).close()
    } catch (error) {
        self.parse_error_list.push(error)
    } finally {
        return find_fb2
    }
}

/**
 * get book cover
 * @return {string} image
 */
Parser_fb2.prototype.get_cover_image = function () {
    if (lib_vconv.isAbsent(this.book) || lib_vconv.isAbsent(this.book.cover_binary_id) || lib_vconv.isAbsent(this.book.binary)) return undefined
    let fnd = this.book.binary.find(f => f.id === this.book.cover_binary_id && f.content_type.includes('image'))
    if (lib_vconv.isAbsent(fnd)) {
        return undefined
    } else {
        return fnd.data
    }
}

/**
 * get book annotation as text
 * @param {type_format} [option]
 * @return {string} formatted text
 */
Parser_fb2.prototype.get_formatted_annotation = function (option) {
    if (lib_vconv.isAbsent(this.book) || lib_vconv.isAbsent(this.book.annotation) || this.book.annotation.length <= 0) return ''
    return get_formatted_text(option, this.book.annotation, this.book.link)
}

/**
 * get book text
 * @param {type_format} [option]
 * @return {string} formatted text
 */
Parser_fb2.prototype.get_formatted_text = function (option) {
    if (lib_vconv.isAbsent(this.book) || lib_vconv.isAbsent(this.book.text) || this.book.text.length <= 0) return ''
    return get_formatted_text(option, this.book.text, this.book.link)
}

/**
 * @private
 * @static
 * @param {string} ch
 * @returns {boolean}
 */
function check_delimiter_char(ch) {
    if (lib_vconv.isAbsent(ch)) false
    if ([' ','\n','\r','\t'].includes(ch.substring(0,1))) {
        return true
    }
    return false
}

/**
 * @private
 * @static
 * @param {type_format} option
 * @param {type_text[]} data
 * @param {type_link[]} links
 * @returns {string}
 */
function get_formatted_text(option, data, links) {
    if (lib_vconv.isEmpty(data)) return ''

    if (lib_vconv.isAbsent(option)) {
        option = {
            format: 'plain',
            indent: '  ',
            eol: false
        }
    } else {
        option.format = lib_vconv.toString(option.format, 'plain')
        option.indent = lib_vconv.toString(option.indent, '  ')
        option.eol = lib_vconv.toBool(option.eol, false)
    }

    /** @type {string[]} */
    let lines = ['']
    /** @type {number} */
    let last_line_index = 0

    lines[last_line_index] = lines[last_line_index].concat(option.indent)

    data.forEach(item => {
        let text = item.data

        if (['plain','markdown'].includes(option.format)) {

            if (option.eol === false) {
                let fnd_eol = text.indexOf(lib_os.EOL)
                while (fnd_eol >= 0) {
                    text = text.substring(0, fnd_eol) + ' ' + text.substring(fnd_eol + 1, text.length).trimLeft()
                    fnd_eol = text.indexOf(lib_os.EOL)
                }
            }

            if (item.new_line_before) {
                lines.push('')
                last_line_index++
                lines[last_line_index] = lines[last_line_index].concat(option.indent)
            }

            if (option.format === 'plain') {
                let link = get_formatted_link(option, item.link, links)
                if (!lib_vconv.isEmpty(link)) {
                    let text_link = text.trim()
                    if (link.substring(0, text_link.length).trim() === text_link) {
                        if (lib_vconv.isAbsent(lib_vconv.toInt(text_link))) {
                            text_link = '* ('.concat(link.trim(),')')
                        } else {
                            text_link = '* ('.concat(link.substring(text_link.length, link.length).trim(),')')
                        }
                    } else {
                        text_link = '* ('.concat(text_link,' - ', link.trim(),')')
                    }
                    lines[last_line_index] = lines[last_line_index].concat(text_link)
                } else {
                    lines[last_line_index] = lines[last_line_index].concat(text)
                }
            } else if (option.format === 'markdown') {

                if (item.path.includes('.poem.') || item.path.includes('.epigraph.')) {
                    if (lib_vconv.isAbsent(item.style_list)) {
                        item.style_list = [{name: 'italic'}]
                    } else {
                        item.style_list.push({name: 'italic'})
                    }
                }

                text = get_styled_text(item)

                if (item.new_line_before === true || last_line_index === 0) {
                    if (item.path === '.title.p') {
                        text = '# '.concat(text)
                    } else if (item.path === '.section.title.p') {
                        text = '## '.concat(text)
                    } else if (item.path.substring(item.path.length - 13, item.path.length) === '.poem.title.p') {
                        text = '##### '.concat(text)
                    }
                }

                lines[last_line_index] = lines[last_line_index].concat(text)
            }
        }
    })
    return lines.join(lib_os.EOL)
}

/**
 * @private
 * @static
 * @param {type_format} option
 * @param {type_link} link
 * @param {type_link[]} links
 * @returns {string}
 */
function get_formatted_link(option, link, links) {
    if (lib_vconv.isAbsent(link) || lib_vconv.isAbsent(links) || links.length <= 0) return ''

    let id = lib_vconv.findPropertyValueInObject(link, 'xlink:href')
    id = lib_vconv.toString(id,'').trim().toLowerCase()
    id = lib_vconv.border_del(id, '#', undefined)
    if (lib_vconv.isEmpty(id)) return ''

    /** @type {type_text[]} */
    let data = links.filter(f => lib_vconv.toString(f.id,'').trim().toLowerCase() === id).map(m => {
        return {
            path: m.path,
            style_list: m.style_list,
            data: m.data,
            link: undefined,
            new_line_before: m.new_line_before
        }
    })
    if (lib_vconv.isAbsent(data) || data.length <= 0) return ''

    return lib_vconv.replaceAll(lib_vconv.toString(get_formatted_text(option, data, undefined), ''), lib_os.EOL, ' ').trim()
}

/**
 *
 * @private
 * @static
 * @param {type_text} item
 * @returns {string}
 */
function get_styled_text(item) {
    if (lib_vconv.isAbsent(item)) return ''

    let text = item.data
    text = lib_vconv.replaceAll(text,'*','\\*')
    text = lib_vconv.replaceAll(text,'_','\\_')
    text = lib_vconv.replaceAll(text,'#','\\#')

    if (lib_vconv.isAbsent(item.style_list) || item.style_list.length <= 0) return text

    let allow_bold = false
    let allow_italic = false

    item.style_list.forEach(style => {
        let name = lib_vconv.toString(lib_vconv.findPropertyValueInObject(style,'name'),'').trim().toLowerCase()
        if (name === 'bold') {
            allow_bold = true
        } else if (name === 'italic') {
            allow_italic = true
        }
    })

    if (allow_bold === false && allow_italic === false) return text

    // let len1 = text.length
    // text = text.trimLeft()
    // let len2 = text.length
    // text = text.trimRight()
    // let len3 = text.length

    let texts = []
    text.split(lib_os.EOL).forEach((t1, t1_index, t1_array) => {
        let t1_length = t1_array.length
        t1.split('\t').forEach((t2, t2_index, t2_array) => {
            let t2_length = t2_array.length
            t2.split(' ').forEach((t3, t3_index, t3_array) => {
                let t3_length = t3_array.length
                if (!lib_vconv.isEmpty(t3)) {
                    if (allow_italic) {
                        t3 = '*'.concat(t3, '*')
                    }
                    if (allow_bold) {
                        t3 = '**'.concat(t3, '**')
                    }
                }
                texts.push(t3)
                if (t3_index + 1 < t3_length) {
                    texts.push(' ')
                }
            })
            if (t2_index + 1 < t2_length) {
                texts.push('\t')
            }
        })
        if (t1_index + 1 < t1_length) {
            texts.push(lib_os.EOL)
        }
    })

    text = texts.join('')

    // if (len1 > len2) {
    //     text = ' '.repeat(len1 - len2).concat(text)
    // }
    // if (len2 > len3) {
    //     text = text.concat(' '.repeat(len2 - len3))
    // }

    return text
}