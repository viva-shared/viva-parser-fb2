## Classes

<dl>
<dt><a href="#Parser_fb2">Parser_fb2</a></dt>
<dd><p>(license MIT) parse text from fb2 format, example - see example.js</p>
</dd>
</dl>

## Typedefs

<dl>
<dt><a href="#type_author">type_author</a></dt>
<dd></dd>
<dt><a href="#type_sequence">type_sequence</a></dt>
<dd></dd>
<dt><a href="#type_text">type_text</a></dt>
<dd></dd>
<dt><a href="#type_link">type_link</a></dt>
<dd></dd>
<dt><a href="#type_binary">type_binary</a></dt>
<dd></dd>
<dt><a href="#type_book">type_book</a></dt>
<dd></dd>
<dt><a href="#type_format">type_format</a></dt>
<dd></dd>
</dl>

<a name="Parser_fb2"></a>

## Parser\_fb2
(license MIT) parse text from fb2 format, example - see example.js

**Kind**: global class  

* [Parser_fb2](#Parser_fb2)
    * [.book](#Parser_fb2+book) : [<code>type\_book</code>](#type_book)
    * [.parse_error_list](#Parser_fb2+parse_error_list) : <code>Array.&lt;Object&gt;</code>
    * [.parse(text)](#Parser_fb2+parse) ⇒ <code>boolean</code>
    * [.get_cover_image()](#Parser_fb2+get_cover_image) ⇒ <code>string</code>
    * [.get_formatted_annotation([option])](#Parser_fb2+get_formatted_annotation) ⇒ <code>string</code>
    * [.get_formatted_text([option])](#Parser_fb2+get_formatted_text) ⇒ <code>string</code>

<a name="Parser_fb2+book"></a>

### parser_fb2.book : [<code>type\_book</code>](#type_book)
result parse fb2-formatted text

**Kind**: instance property of [<code>Parser\_fb2</code>](#Parser_fb2)  
<a name="Parser_fb2+parse_error_list"></a>

### parser_fb2.parse\_error\_list : <code>Array.&lt;Object&gt;</code>
rlist of parsing error

**Kind**: instance property of [<code>Parser\_fb2</code>](#Parser_fb2)  
<a name="Parser_fb2+parse"></a>

### parser_fb2.parse(text) ⇒ <code>boolean</code>
parse fb2-formatted text

**Kind**: instance method of [<code>Parser\_fb2</code>](#Parser_fb2)  
**Returns**: <code>boolean</code> - fb2 or not fb2  

| Param | Type | Description |
| --- | --- | --- |
| text | <code>string</code> | fb2-formatted text |

<a name="Parser_fb2+get_cover_image"></a>

### parser_fb2.get\_cover\_image() ⇒ <code>string</code>
get book cover

**Kind**: instance method of [<code>Parser\_fb2</code>](#Parser_fb2)  
**Returns**: <code>string</code> - image  
<a name="Parser_fb2+get_formatted_annotation"></a>

### parser_fb2.get\_formatted\_annotation([option]) ⇒ <code>string</code>
get book annotation as text

**Kind**: instance method of [<code>Parser\_fb2</code>](#Parser_fb2)  
**Returns**: <code>string</code> - formatted text  

| Param | Type |
| --- | --- |
| [option] | [<code>type\_format</code>](#type_format) | 

<a name="Parser_fb2+get_formatted_text"></a>

### parser_fb2.get\_formatted\_text([option]) ⇒ <code>string</code>
get book text

**Kind**: instance method of [<code>Parser\_fb2</code>](#Parser_fb2)  
**Returns**: <code>string</code> - formatted text  

| Param | Type |
| --- | --- |
| [option] | [<code>type\_format</code>](#type_format) | 

<a name="type_author"></a>

## type\_author
**Kind**: global typedef  
**Properties**

| Name | Type |
| --- | --- |
| last_name | <code>string</code> | 
| middle_name | <code>string</code> | 
| first_name | <code>string</code> | 

<a name="type_sequence"></a>

## type\_sequence
**Kind**: global typedef  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| title | <code>string</code> | series name |
| position | <code>number</code> | position in series |

<a name="type_text"></a>

## type\_text
**Kind**: global typedef  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| path | <code>string</code> | xml path |
| style_list | <code>Array.&lt;Object&gt;</code> | styles |
| data | <code>string</code> | piece of text |
| link | <code>Object</code> | link to note |
| new_line_before | <code>boolean</code> | add or not new line before this piece of text |

<a name="type_link"></a>

## type\_link
**Kind**: global typedef  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| id | <code>string</code> | id link |
| path | <code>string</code> | xml path |
| style_list | <code>Array.&lt;Object&gt;</code> | styles |
| data | <code>string</code> | piece of link |
| new_line_before | <code>boolean</code> | add or not new line before this piece of link |

<a name="type_binary"></a>

## type\_binary
**Kind**: global typedef  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| id | <code>string</code> | id binary resource |
| content_type | <code>string</code> | content type |
| data | <code>string</code> | binary value |

<a name="type_book"></a>

## type\_book
**Kind**: global typedef  
**Properties**

| Name | Type |
| --- | --- |
| title | <code>string</code> | 
| subtitle | <code>string</code> | 
| note | <code>string</code> | 
| origin_author | [<code>type\_author</code>](#type_author) | 
| origin_language | <code>string</code> | 
| translator_author | [<code>type\_author</code>](#type_author) | 
| translator_language | <code>string</code> | 
| sequence_list | [<code>Array.&lt;type\_sequence&gt;</code>](#type_sequence) | 
| genre_list | <code>Array.&lt;string&gt;</code> | 
| keyword_list | <code>Array.&lt;string&gt;</code> | 
| isbn | <code>string</code> | 
| cover_binary_id | <code>string</code> | 
| annotation | [<code>Array.&lt;type\_text&gt;</code>](#type_text) | 
| text | [<code>Array.&lt;type\_text&gt;</code>](#type_text) | 
| link | [<code>Array.&lt;type\_link&gt;</code>](#type_link) | 
| binary | [<code>Array.&lt;type\_binary&gt;</code>](#type_binary) | 

<a name="type_format"></a>

## type\_format
**Kind**: global typedef  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| [format] | <code>string</code> | 'plain' or 'markdown', default 'plain' |
| [indent] | <code>string</code> | indent for new paragraph, default '  ' (2 spaces) |
| [eol] | <code>boolean</code> | allow end-of-line inside paragraph, default false |

